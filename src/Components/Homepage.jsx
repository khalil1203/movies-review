import React from "react";
import CommonList from "./CommonList";

const Homepage = ({ movies, setMovies }) => {

  function changeState(e, title) {
    if (e == "towatch") {
      console.log("hello from towatch");
      let tempArr = movies.map((entry) => {
        if (entry.Title == title) {
            return {
                ...entry,
                toWatch : true,
                willWatch : false,
                watching : false
            }

        }
        return entry;
      });
      setMovies([...tempArr]);
    } else if (e == "willwatch") {
      console.log("hello from willwatch");
      let tempArr = movies.map((entry) => {
        if (entry.Title == title) {
            return {
                ...entry,
                toWatch : false,
                willWatch : true,
                watching : false
            }

        }
        return entry;
      });
      setMovies([...tempArr]);
    } else if (e == "watching") {
      console.log("hello from watching");
      let tempArr = movies.map((entry) => {
        if (entry.Title == title) {
            return {
                ...entry,
                toWatch : false,
                willWatch : false,
                watching : true
            }
        }
        return entry;
      });
      setMovies([...tempArr]);
    }
  }
  changeState();
  return (
    <div>
      <div className="toWatch">
        <h1>To Watch</h1>
        <CommonList
          movies={movies.filter((entry) => entry.toWatch)}
          changeState={changeState}
        />
      </div>

      <div className="willWatch">
        <h1>Will Be Watching</h1>
        <CommonList
          movies={movies.filter((entry) => entry.willWatch)}
          changeState={changeState}
        />
      </div>
      <div className="watching">
        <h1>Currently Watching</h1>
        <CommonList
          movies={movies.filter((entry) => entry.watching)}
          changeState={changeState}
        />
      </div>
    </div>
  );
};

export default Homepage;
