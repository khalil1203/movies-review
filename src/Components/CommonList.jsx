import React from "react";

const CommonList = ({ movies,changeState }) => {
  return (
    <div className="wrap">
      {movies.map((entry) => {
        return (
          <div key={entry.Title}>
            {entry.Title}
            <select key={entry.Title} onChange={(e)=> changeState(e.target.value,entry.Title)}>
              <option value="default">Choose</option>
              <option value="towatch">To Watch</option>
              <option value="willwatch">Will Watch</option>
              <option value="watching">Watching</option>
            </select>
          </div>
        );
      })}
    </div>
  );
};

export default CommonList;
