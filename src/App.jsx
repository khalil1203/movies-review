import { useEffect, useState } from "react";
import "./App.css";
import arr from "./assets/movies.json";
import Homepage from "./Components/Homepage";

function App() {
  let tempArr = arr.map((entry)=>{
    return {
      ...entry,
      toWatch:true,
      willWatch:false,
      watching:false
    }
  });
  const [movies, setMovies] = useState(tempArr);

  return <>
  {console.log(movies)}
  <Homepage movies={movies} setMovies={setMovies} />
  </>;
}

export default App;
